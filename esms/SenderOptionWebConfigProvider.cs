﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using esms.Interfaces;

namespace esms
{
    public class SenderOptionWebConfigProvider : ISenderOptionProvider
    {
        public SendOption GetSenderOption()
        {
            var apiKey = ConfigurationManager.AppSettings["APIKey"];
            var secretKey = ConfigurationManager.AppSettings["SecretKey"];
            var url = ConfigurationManager.AppSettings["Url"];
            var SMSTYPE = ConfigurationManager.AppSettings["SMSTYPE"];

            var option = new SendOption()
            {
                APIKey = apiKey,
                SecretKey = secretKey,
                SMSType = SMSTYPE,
                Url = url
            };
            return option;
        }
    }
}
