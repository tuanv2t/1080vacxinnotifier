﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace esms
{
    public class SendOption
    {
        public string APIKey { get; set; }
        public string SecretKey { get; set; }
        public string Url { get; set; }
        public string SMSType { get; set; }
    }
}
