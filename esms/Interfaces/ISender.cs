﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace esms.Interfaces
{
    public interface ISender
    {
        void Send(string phone, string message);
    }
}
