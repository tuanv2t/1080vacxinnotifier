﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using _1080VacxinNotifier.Interfaces;

namespace _1080VacxinNotifier
{
    public class MessageXMLProvider : IMessageDataProvider
    {
        private XmlDocument Document;
        private string FilePath = "Data\\MessageDB.xml";
        private void LoadData()
        {
            string xml = System.IO.File.ReadAllText(FilePath);
            Document = new XmlDocument();
            Document.LoadXml(xml);
        }

        public bool IsExisting(string phone, string message)
        {
            //Get element by tag
            if (Document == null)
            {
                LoadData();
            }

            XmlNodeList messageList = Document.GetElementsByTagName("Message");
            foreach (XmlNode messageNode in messageList)
            {
                //get child node
                string phoneValue = string.Empty;
                string contentValue = string.Empty;
                foreach (XmlNode childNode in messageNode.ChildNodes)
                {
                    if (childNode.Name == "Phone")
                    {
                        phoneValue = childNode.InnerText.ToLower();

                    }
                    if (childNode.Name == "Content")
                    {
                        contentValue = childNode.InnerText.ToLower();

                    }
                }
                if (!string.IsNullOrWhiteSpace(phoneValue) && !string.IsNullOrWhiteSpace(contentValue))
                {
                    if (phone.ToLower().Equals(phoneValue) && message.ToLower().Equals(contentValue))
                    {
                        return true;
                    }
                }
               
            }
            return false;
        }

        public List<MessageModel> GetData()
        {
            var data = new List<MessageModel>();

            if (Document == null)
            {
                LoadData();
            }

            XmlNodeList messageList = Document.GetElementsByTagName("Message");
            foreach (XmlNode messageNode in messageList)
            {
                //get child node
                string phoneValue = string.Empty;
                string contentValue = string.Empty;
                string createdDate = string.Empty;
                foreach (XmlNode childNode in messageNode.ChildNodes)
                {
                    if (childNode.Name == "Phone")
                    {
                        phoneValue = childNode.InnerText.ToLower();

                    }
                    if (childNode.Name == "Content")
                    {
                        contentValue = childNode.InnerText;

                    }
                     if (childNode.Name == "DateTime")
                    {
                        createdDate = childNode.InnerText;

                    }
                }
                if (!string.IsNullOrEmpty(phoneValue)
                    && !string.IsNullOrEmpty(contentValue))
                {
                    var message = new MessageModel()
                    {
                        Phone = phoneValue,
                        Content = contentValue,
                        CreatedDate = DateTime.Parse(createdDate)
                    };
                    data.Add(message);
                }
            }
            return data;
        }

        public void Add(string phone, string message, DateTime sentDate)
        {
            phone = phone.RemoveInvalidXmlChars();
            message = message.RemoveInvalidXmlChars();
            XmlDocument xd = new XmlDocument();
            string xml = System.IO.File.ReadAllText(FilePath);
            xd.LoadXml(xml);
          
            XmlElement messageNode = xd.CreateElement("Message");

            XmlElement contentNode = xd.CreateElement("Content");
            contentNode.InnerText = message;

            XmlElement phoneNode = xd.CreateElement("Phone");
            phoneNode.InnerText = phone;
            XmlElement dateTimeNode = xd.CreateElement("DateTime");
            dateTimeNode.InnerText = sentDate.ToString("yyyy-MM-dd HH:mm:ss.fff");

            messageNode.AppendChild(phoneNode);
            messageNode.AppendChild(contentNode);
            messageNode.AppendChild(dateTimeNode);

            //get root
            XmlNode root = xd.GetElementsByTagName("Messages")[0];
            root.AppendChild(messageNode);
            xd.Save(FilePath);
        }
        
    }
}
