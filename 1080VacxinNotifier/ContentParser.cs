﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using _1080VacxinNotifier.Interfaces;
using HtmlDocument = HtmlAgilityPack.HtmlDocument;

namespace _1080VacxinNotifier
{
    public class ContentParser:IContentParser
    {
        public List<string> GetVacxinArtical(string content)
        {
            var result = new List<string>();
            HtmlDocument document = new HtmlDocument();
            document.LoadHtml(content);
            var aList = document.DocumentNode.Descendants("a").ToList();
            aList.AddRange(document.DocumentNode.Descendants("A").ToList());
            foreach (var a in aList)
            {
                //
                if (a.Attributes["title"] != null)
                {
                    var title = a.Attributes["title"].Value;
                    if (title != null)
                    {
                        if (title.ToLower().Contains("VẮC XIN".ToLower()) ||
                            title.ToLower().Contains("VACXIN".ToLower()) ||
                            title.ToLower().Contains("6 TRONG 1".ToLower())||
                            title.ToLower().Contains("5 TRONG 1".ToLower()))
                        {
                            result.Add(title);
                        }
                    }
                }
                
                var text = a.InnerText;
                if (text != null)
                {
                    if (text.ToLower().Contains("VẮC XIN".ToLower()) ||
                        text.ToLower().Contains("VACXIN".ToLower()) ||
                        text.ToLower().Contains("6 TRONG 1".ToLower())||
                        text.ToLower().Contains("5 TRONG 1".ToLower()))
                    {
                        result.Add(text);
                    }
                }
            }
            return result.Distinct().ToList();
        }
    }
}
