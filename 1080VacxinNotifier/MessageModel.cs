﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _1080VacxinNotifier
{
    public class MessageModel
    {
        public string Phone { get; set; }
        public string Content { get; set; }
        public DateTime CreatedDate { get; set; }
    }
}
