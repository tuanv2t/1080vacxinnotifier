﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.IO;
using _1080VacxinNotifier.Interfaces;

namespace _1080VacxinNotifier
{
    public class Request : IRequest
    {
        public string GetWebContent(string url)
        {
            WebRequest request = WebRequest.Create(url);
            request.Timeout = 60*60*1000;//60 minutes
            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            Stream dataStream = response.GetResponseStream();
            var content = string.Empty;
            using (var reader = new StreamReader(dataStream))
            {
                content = reader.ReadToEnd();
            }
            response.Close();
            dataStream.Close();
            return content;

        }
    }
}
