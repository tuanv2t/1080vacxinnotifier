﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace _1080VacxinNotifier
{
    public static class ExtensionMethod
    {
        public static string RemoveInvalidXmlChars(this string text)
        {
            if (string.IsNullOrWhiteSpace(text))
            {
                return text;
            }
            char[] validXmlChars = text.Where(ch => XmlConvert.IsXmlChar(ch)).ToArray();
            return new string(validXmlChars);
        }
    }
}
