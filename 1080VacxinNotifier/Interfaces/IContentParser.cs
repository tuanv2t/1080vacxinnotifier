﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _1080VacxinNotifier.Interfaces
{
    public interface IContentParser
    {
        List<string> GetVacxinArtical(string content);
    }
}
