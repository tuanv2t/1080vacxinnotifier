﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _1080VacxinNotifier.Interfaces
{
    public interface IMessageDataProvider
    {
        List<MessageModel> GetData();
        bool IsExisting(string phone, string message);
        void Add(string phone, string message, DateTime sentDate);
    }
}
