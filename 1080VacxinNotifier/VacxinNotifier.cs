﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using esms;
using esms.Interfaces;
using _1080VacxinNotifier.Interfaces;

namespace _1080VacxinNotifier
{
    public class VacxinNotifier
    {
        private readonly IRequest _request;
        private readonly IContentParser _contentParser;
        private readonly ISender _sender;
        private readonly IMessageDataProvider _messageDataProvider;
        public VacxinNotifier(IRequest request, IContentParser contentParser, ISender sender,
            IMessageDataProvider messageDataProvider)
        {
            _request = request;
            _contentParser = contentParser;
            _sender = sender;
            _messageDataProvider = messageDataProvider;
        }
        public  List<string> GetVacxinArtical()
        {
            var content = _request.GetWebContent("http://108hcm.com.vn/");
            var result = _contentParser.GetVacxinArtical(content);
            return result;
        }

      
        public  void SendSMS()
        {
            var messageList = GetVacxinArtical();
            var receivedPhones = ConfigurationManager.AppSettings["ReceivedPhones"];

            //var message = string.Empty;
            //if (result.Count > 0)
            //{
            //    message = string.Join(Environment.NewLine, result);
            //}
            foreach (var message in messageList)
            {
                if (!string.IsNullOrWhiteSpace(message))
                {

                    var phoneList = receivedPhones.Split(new char[] { ',' }).ToList();
                    foreach (var phone in phoneList)
                    {
                        if (!_messageDataProvider.IsExisting(phone, message))
                        {
                            _sender.Send(receivedPhones, message);
                            _messageDataProvider.Add(phone, message, DateTime.Now);
                        }

                    }

                }
            }
           
        }

        public List<MessageModel> GetData()
        {
            return _messageDataProvider.GetData();
        }
    }
}
