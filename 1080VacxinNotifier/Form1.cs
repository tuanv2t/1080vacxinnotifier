﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Timers;
using esms;
using Microsoft.Win32;

namespace _1080VacxinNotifier
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private static VacxinNotifier _vacxinNotifier;
        private static bool sendSM=false;
        private static ListView ListView;
        private void Form1_Load(object sender, EventArgs e)
        {
            var senderOption = new SenderOptionWebConfigProvider().GetSenderOption();
            _vacxinNotifier = new VacxinNotifier(new Request(), new ContentParser(), new Sender(senderOption), new MessageXMLProvider());
            ListView = listView1;
            int intervalMinute = 60; //default 60 minutes
            try
            {
                intervalMinute = Int32.Parse(ConfigurationManager.AppSettings["IntervalMinute"]);
            }
            catch
            {
                intervalMinute = 60;
            }
            try
            {
                sendSM = bool.Parse(ConfigurationManager.AppSettings["SendSMS"]);
            }
            catch
            {
                sendSM = false;
            }

            System.Timers.Timer timer = new System.Timers.Timer(intervalMinute * 60 * 1000); //change minutes to milisecond
            timer.Elapsed += (sender1, e1) => HandleTimer();
            timer.Enabled = true;
            timer.Start();
            this.WindowState = FormWindowState.Minimized;
            AddApplicationToStartup();
            ShowMessage();
            if (sendSM)
            {
                SendSMS();
            }
            
        }

        private static void HandleTimer()
        {
            ShowMessage();
            if (sendSM)
            {
                  SendSMS();
            }
          
        }

        private static void SendSMS()
        {
            _vacxinNotifier.SendSMS();
            //show on list view
              ListView.Items.Clear();
            //display to listview
            var data = _vacxinNotifier.GetData().OrderByDescending(x => x.CreatedDate).ToList();
            foreach (var messageModel in data)
            {
                var item = new ListViewItem(messageModel.Content);
                item.SubItems.Add(messageModel.Phone);
                item.SubItems.Add(messageModel.CreatedDate.ToLongDateString());
                ListView.Items.Add(item);
            }
        }
        private static void ShowMessage()
        {
            var result = _vacxinNotifier.GetVacxinArtical();
            if (result.Count > 0)
            {
                MessageBox.Show(string.Join(Environment.NewLine, result));
            }
            else
            {
                MessageBox.Show("Chua tim thay thong bao tiem vaxin");
            }
        }

        public static void AddApplicationToStartup()
        {
            using (
                RegistryKey key = Registry.CurrentUser.OpenSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run",
                    true))
            {
                key.SetValue("Vacxin Notifier", "\"" + Application.ExecutablePath + "\"");
            }
        }
    }

}
