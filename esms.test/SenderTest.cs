﻿using System;
using System.Configuration;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace esms.test
{
    [TestClass]
    public class SenderTest
    {
        [TestMethod]
        public void SendSMS_OK()
        {
            var apiKey = ConfigurationManager.AppSettings["APIKey"];
            var secretKey = ConfigurationManager.AppSettings["SecretKey"];
            var url = ConfigurationManager.AppSettings["Url"];
            var SMSTYPE = ConfigurationManager.AppSettings["SMSTYPE"];
            var receivedPhones = ConfigurationManager.AppSettings["ReceivedPhones"];
            var message = ConfigurationManager.AppSettings["Message"];

            Sender sender = new Sender(new SendOption()
            {
                APIKey = apiKey,
                SecretKey = secretKey,
                SMSType = SMSTYPE,
                Url = url
            });
            sender.Send(receivedPhones, message);

        }
    }
}
