﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace _1080VacxinNotifier.Test
{
    [TestClass]
    public class MessageXMLProviderTest
    {
        [TestMethod]
        public void Add_IsExisting_OK()
        {
            string phone = "111111111";
            string message = "TesT";
            var messageXMLProvider = new MessageXMLProvider();
            messageXMLProvider.Add(phone, message, DateTime.Now);
            var isExisting = messageXMLProvider.IsExisting(phone, message);
            Assert.AreEqual(isExisting,true);
        }
    }
}
