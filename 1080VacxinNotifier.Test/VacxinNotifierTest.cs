﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using esms;
using esms.Interfaces;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace _1080VacxinNotifier.Test
{
    [TestClass]
    public class VacxinNotifierTest
    {
        [TestMethod]
        public void SendSMS_OK()
        {
            var sender = new FakeSender();
            var vacxinNotifier = new VacxinNotifier(new Request(), new ContentParser(), new FakeSender(), new MessageXMLProvider());
            vacxinNotifier.SendSMS();
        }

        class FakeSender:ISender
        {

            public void Send(string phone, string message)
            {
                return;
            }
        }
    }
}
